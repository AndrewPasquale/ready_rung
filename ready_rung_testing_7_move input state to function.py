# -*- coding: utf-8 -*-
"""
Created on Sat Dec  5 09:00:45 2020

@author: Andrew
"""

import os
import time
from datetime import datetime
import multiprocessing as mp
import threading as td
import sys
import json

# replace the below when integrating, so instead of Spyder variable editor, read from actual comms tasks
L1_Comms_Ok=True
L1_Rotation_Warning=False
L1_Rotation_Fault=False
L1_Lens_Dirty=False
L1_Calibration_Targets_Ok=True
L1_Starting_Up=True
L1_Lidar_Healthy=True
L1_Temperature_Fault=False



'''
### Function: Alarm  ###
  - Purpose:
    - Accepts an alarm input and then
        --Applies a debounce time to prevent nuisance trips
        --Sets the AlmTripped tag once debouceON time has expired.
            And writes a record to the log file for the time of trip. (JSON, SQL database etc)
    - Can also be used for warnings and general logging of events
  - Revision: 0.1
  - Who/When: 20201205, A.Pasquale
  --Todo
    --Replace csv logs with with SQL writes.    
'''       
class AlarmClass:
    def __init__(self, Alm_Name, Type, debounceON, debounceOff, Message):
        self.Alm_Name = Alm_Name   
        self.Type = Type    # string for database filtering, e.g.'Trip', 'Warning', 'Method' active, 'Debug' info
        self.debounceON = debounceON  #how long the input must be active for before logging to file
        self.debounceOff = debounceOff #how long to keep the trip active for after the input becomes healthy. Is to stop flickering for zero debounceOn trips
        self.Message = Message   #Message to log to file
        self.AlmFaultedLatch = False
        self.AlmTripLatch = False
        self.AlmTripped = False
        self.FaultedTime =  datetime.now() - datetime.now()
        self.TimesinceFirstFault =  datetime.now() - datetime.now()
        self.DebounceTime = datetime.now() - datetime.now()
        self.TimeCameHealthy = datetime.now()
        self.TimesinceCameHly = datetime.now() - datetime.now()
        
    def Almfunc(self, InputState):
        if InputState:            #If true=Log now, If false = no action, also known as 'Input Fault state' when used interlock logic
            if not self.AlmFaultedLatch:
                self.AlmFaultedLatch =True    
                print(str(datetime.now()) + " ; Debug ; " + self.Alm_Name + " ; Input state went to fault ")   #Todo: replace with write to SQL/JSON
                with open("Logfiles.csv", 'a') as fw:  #Todo: replace with write to SQL/JSON
                    fw.write(str(datetime.now()) + " ; Debug ; " + self.Alm_Name + " ; Input went to fault " + "\n")						
                self.FaultedTime = datetime.now()
        
            self.TimesinceFirstFault = datetime.now() - self.FaultedTime
            
            if float(self.TimesinceFirstFault.seconds)>self.debounceON:
                self.AlmTripped =True
                if not self.AlmTripLatch:
                    print(str(datetime.now()) + " ; " + self.Type + " ; " + self.Alm_Name + " ; ACTIVATED ;  " + self.Message + "\n") #Todo: replace with write to SQL/JSON
                    with open("Logfiles.csv", 'a') as fw:  #Todo: replace with write to SQL/JSON
                        fw.write(str(datetime.now()) + " ; " + self.Type + " ; " + self.Alm_Name + " ; ACTIVATED ;  " + self.Message + "\n")
                    self.AlmTripLatch=True
        else:
            if self.AlmTripped:
                if self.AlmFaultedLatch:
                    self.TimeCameHealthy = datetime.now()
                    print(str(datetime.now()) + " ; Debug ; " + self.Alm_Name + " ; Input went healthy ")   #Todo:replace with write to SQL/JSON
                    with open("Logfiles.csv", 'a') as fw:  #Todo: replace with write to SQL/JSON
                        fw.write(str(datetime.now()) + " ; Debug ; " + self.Alm_Name + " ; Input went healthy " + "\n")
                    self.TimesinceCameHly = datetime.now() - self.TimeCameHealthy
       
                if float(self.TimesinceCameHly.seconds)>self.debounceOff:
                    self.AlmTripped = False
                    print(str(datetime.now()) + " ; " + self.Type + " ; " + self.Alm_Name + " ; DEACTIVATED " + "\n") #Todo: replace with write to SQL/JSON logs
                    with open("Logfiles.csv", 'a') as fw:  #Todo: replace with write to SQL/JSON
                        fw.write(str(datetime.now()) + " ; " + self.Type + " ; " + self.Alm_Name + " ; DEACTIVATED " + "\n")
                else:
                    self.TimesinceCameHly = datetime.now() - self.TimeCameHealthy
            
            self.AlmFaultedLatch = False
            self.AlmTripLatch =  False  
        return self.AlmTripped

def mp_main():
    #initialise variables
    L1_ReadyLatch=True
    SPMS_ReadyLatch=False

    '''
        Do all the normal comms and measurement functions. Then assess outputs to send over the PLC interface and send to HMI.
        ....
        ...
        ...
    '''

    #Initialise alarm objects
    if 1:
        L1_Comms_Ok_OBJ = AlarmClass("L1_Comms_Ok", "Trip", 5, 2, "L1 Comms Not Ok, Investigate")
        L1_Lidar_Healthy_OBJ = AlarmClass("L1_Lidar_Healthy", "Trip", 5, 2, "..., Investigate")   #Todo: give a proper message instruction later. Low priority
        L1_Starting_Up_OBJ = AlarmClass("L1_Starting_Up", "Trip", 5, 2, "..., Investigate")   #Todo: give a proper message instruction later. Low priority
        L1_Temperature_Fault_OBJ = AlarmClass("L1_Temperature_Fault", "Trip", 5, 2, "..., Investigate")   #Todo: give a proper message instruction later. Low priority
        L1_Rotation_Warning_OBJ = AlarmClass("L1_Rotation_Warning", "Warning", 5, 2, "...Investigate")   #Todo: give a proper message instruction later. Low priority
        L1_Rotation_Fault_OBJ = AlarmClass("L1_Rotation_Fault", "Trip", 5, 2, "..., Investigate")   #Todo: give a proper message instruction later. Low priority
        L1_Lens_Dirty_OBJ = AlarmClass("L1_Lens_Dirty", "Trip", 5, 2, "..., Investigate")   #Todo: give a proper message instruction later. Low priority
        L1_Calibration_Targets_Ok_OBJ = AlarmClass("L1_Calibration_Targets_Ok", "Trip", 5, 2, "..., Investigate")   #Todo: give a proper message instruction later. Low priority

    print ("Start monitoring interlock inputs. \n")
    while True:
        
        try:        #Todo: replace with actual input tag from lidar comms, instead of this Json read being used for testing purposes.
            with open('ready_rung_alarms.json', 'r') as fr:
                L1_Comms_Ok, L1_Lidar_Healthy, L1_Starting_Up, L1_Temperature_Fault,L1_Rotation_Warning,L1_Rotation_Fault, L1_Lens_Dirty, L1_Calibration_Targets_Ok = json.load(fr)
        
        except IOError:
            print('ready_rung_alarms.json failed')
        '''       
        print("L1_Comms_Ok " + str(L1_Comms_Ok))
        print("L1_Lidar_Healthy " + str(L1_Lidar_Healthy))
        print("L1_Starting_Up " + str(L1_Starting_Up))
        print("L1_Temperature_Fault " + str(L1_Temperature_Fault))'''
                
        ### Monitor Interlock inputs, set alarm trip states, logging to Json/SQL
            # Performs debounce timer function, and alarm stretch timer for use in ready rungs            
        L1_Comms_Ok_OBJ.Almfunc(not L1_Comms_Ok)        #Todo: replace arguements with actual input tag from lidar comms
        L1_Lidar_Healthy_OBJ.Almfunc(not L1_Lidar_Healthy)
        L1_Starting_Up_OBJ.Almfunc(L1_Starting_Up)
        L1_Temperature_Fault_OBJ.Almfunc(L1_Temperature_Fault)
        L1_Rotation_Warning_OBJ.Almfunc(L1_Rotation_Warning)
        L1_Rotation_Fault_OBJ.Almfunc(L1_Rotation_Fault)
        L1_Lens_Dirty_OBJ.Almfunc(L1_Lens_Dirty)
        L1_Calibration_Targets_Ok_OBJ.Almfunc(not L1_Calibration_Targets_Ok)


        ###Combine interlock bits in function health status    (just to reduce ready rung length for the end functions... ie wrap up where makes sense to do so)
        
        ###L1 healthy
        if 1:
            L1_Ready=True   #set as TRUE, then any IL that is tripped, set FALSE
            
            ###L1 Ready Interlocks.     Laid out this way to make easier to read and to add to as required.
            if L1_Comms_Ok_OBJ.AlmTripped: L1_Ready=False
            if L1_Starting_Up_OBJ.AlmTripped: L1_Ready=False
            if L1_Lidar_Healthy_OBJ.AlmTripped: L1_Ready=False
            if L1_Temperature_Fault_OBJ.AlmTripped: L1_Ready=False
            #if L1_Rotation_Warning_OBJ.AlmTripped: L1_Ready=False   #warning only, not interlock
            if L1_Rotation_Fault_OBJ.AlmTripped: L1_Ready=False
            if L1_Lens_Dirty_OBJ.AlmTripped: L1_Ready=False   
            if L1_Calibration_Targets_Ok_OBJ.AlmTripped: L1_Ready=False
            
            #log state transitions to json or SQL
            if not L1_Ready:
                if not L1_ReadyLatch:
                    L1_ReadyLatch=True
                    with open("Logfiles.csv", 'a') as fw:  #Todo: replace with write to SQL/JSON
                        fw.write(str(datetime.now()) + " ; Debug ; " + "L1_Ready =" + str(L1_Ready) + "\n")
            else:
                if L1_ReadyLatch:
                    with open("Logfiles.csv", 'a') as fw:  #Todo: replace with write to SQL/JSON
                        fw.write(str(datetime.now()) + " ; Debug ; " + "L1_Ready =" + str(L1_Ready) + "\n")
                L1_ReadyLatch = False
        ###L2 healthy
        #...as above
        L2_Ready=True   #set as TRUE, then any IL that is tripped, set FALSE
        
        ###L3 healthy
            #...as above
        L3_Ready=True   #set as TRUE, then any IL that is tripped, set FALSE
        
        ###L4 healthy
            #...as above
        L4_Ready=True   #set as TRUE, then any IL that is tripped, set FALSE
        
        #SPMS Inputs       
        SPMS_Function_Active = True   # repalce with logic that checks the period since last function update < 200ms.
        SPMS_Measurement_Error = False # ...
        Trolley_Detected = True   # ...
        Spreader_Detected = True
        SPMS_PLC_Consistency_Fault = False
        Lidar_Consistency_Fault = False
        SPMS_Calibration_Ready = True
        
        # SPMS drive ready 
        if 1:
            SPMS_Ready=True
            
            #Check SPMS Ready Interlocks.   Laid out this way to make easier to read and to add to as required.
            if not L1_Ready: SPMS_Ready=False
            if not L2_Ready: SPMS_Ready=False
            if not L3_Ready: SPMS_Ready=False
            if not SPMS_Function_Active: SPMS_Ready=False
            if SPMS_Measurement_Error: SPMS_Ready=False
            if not Trolley_Detected: SPMS_Ready=False
            if not Spreader_Detected: SPMS_Ready=False
            if SPMS_PLC_Consistency_Fault: SPMS_Ready=False
            if Lidar_Consistency_Fault: SPMS_Ready=False
            if not SPMS_Calibration_Ready: SPMS_Ready=False

            #log state transitions to json or SQL
            if not SPMS_Ready:
                if not SPMS_ReadyLatch:
                    SPMS_ReadyLatch=True
                    print("SPMS_Ready =" + str(SPMS_Ready) + " "  + str(datetime.now())  + "\n")
                    with open("Logfiles.csv", 'a') as fw:  #Todo: replace with write to SQL/JSON
                        fw.write(str(datetime.now()) + " ; Debug ; " + "SPMS_Ready =" + str(SPMS_Ready) + "\n")
            else:
                if SPMS_ReadyLatch:
                    print("SPMS_Ready =" + str(SPMS_Ready) + "  all happy at " + str(datetime.now())  + "\n" )
                    with open("Logfiles.csv", 'a') as fw:  #Todo: replace with write to SQL/JSON
                        fw.write(str(datetime.now()) + " ; Debug ; " + "SPMS_Ready =" + str(SPMS_Ready) + "\n")
                SPMS_ReadyLatch = False
        
        # LCPS drive ready 
        ...
        # SPSS drive ready 
        ...
        # FLS drive ready 
        ...
        # TPS drive ready     
        ...
        
        #Call the function to send this over the OPC interface to the PLC.
        #Display this on the HMI status page.
        
        time.sleep(1)

if __name__ == '__main__':
	mp.freeze_support() # For PyInstaller to work properly
	try:
		mp_main()
		while td.active_count() > 0:
			time.sleep(0.1)
	except KeyboardInterrupt:
		exit_app = True
		sys.exit()
		raise
